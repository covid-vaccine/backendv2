drop database cetasdb;
drop user cetas;
create user cetas with password 'password';
create database cetasdb with template=template0 owner=cetas;
\connect cetasdb;
alter default privileges grant all on tables to cetas;
alter default privileges grant all on sequences to cetas;

create table ul_users(
user_id integer primary key not null,
first_name varchar(50) not null,
last_name varchar(50) not null,
email varchar(80) not null,
password text not null
);

create table ul_registrants(
registrant_id integer primary key not null,
user_id integer not null,
registrant_name varchar(100) not null,
gender varchar(10) not null,
nik varchar(20) not null,
age int not null,
address text not null,
work_type varchar(50) not null,
work_duration int not null,
disease_history char(50) not null
);
alter table ul_registrants add constraint registrant_users_fk foreign key (user_id) references ul_users(user_id);

create table ul_tickets(
ticket_id integer primary key not null,
registrant_id integer not null,
location varchar(100) not null,
attempt_number integer not null,
release_date TIMESTAMP not null,
fee varchar(20) not null
);
alter table ul_tickets add constraint ticket_registrants_fk foreign key (registrant_id) references ul_registrants(registrant_id);

create sequence ul_users_seq increment 1 start 1;
create sequence ul_registrants_seq increment 1 start 1;
create sequence ul_tickets_seq increment  1 start 1;