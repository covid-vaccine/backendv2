package com.outlanders.cetas.domain;

import java.sql.Date;

public class Result {
    private Integer ticket_id;
    private String name;
    private String nik;
    private String address;
    private String email;
    private String Location;
    private Integer attempt_number;
    private Date release_date;
    private String fee;

    public Result(Integer ticket_id, String name, String nik, String address, String email, String location, Integer attempt_number, Date release_date, String fee) {
        this.ticket_id = ticket_id;
        this.name = name;
        this.nik = nik;
        this.address = address;
        this.email = email;
        Location = location;
        this.attempt_number = attempt_number;
        this.release_date = release_date;
        this.fee = fee;
    }

    public Integer getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(Integer ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public Integer getAttempt_number() {
        return attempt_number;
    }

    public void setAttempt_number(Integer attempt_number) {
        this.attempt_number = attempt_number;
    }

    public Date getRelease_date() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }
}