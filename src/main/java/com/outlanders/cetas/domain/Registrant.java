package com.outlanders.cetas.domain;

public class Registrant {
    private Integer registrantId;
    private Integer user_id;
    private String registrantName;
    private String gender;
    private String nik;
    private Integer age;
    private String address;
    private String workType;
    private Integer workDuration;
    private String diseaseHistory;

    public Registrant(Integer registrantId, Integer user_id, String registrantName, String gender, String nik, Integer age, String address, String workType, Integer workDuration, String diseaseHistory) {
        this.registrantId = registrantId;
        this.user_id = user_id;
        this.registrantName = registrantName;
        this.gender = gender;
        this.nik = nik;
        this.age = age;
        this.address = address;
        this.workType = workType;
        this.workDuration = workDuration;
        this.diseaseHistory = diseaseHistory;
    }

    public Integer getRegistrantId() {
        return registrantId;
    }

    public void setRegistrantId(Integer registrantId) {
        this.registrantId = registrantId;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getRegistrantName() {
        return registrantName;
    }

    public void setRegistrantName(String registrantName) {
        this.registrantName = registrantName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public Integer getWorkDuration() {
        return workDuration;
    }

    public void setWorkDuration(Integer workDuration) {
        this.workDuration = workDuration;
    }

    public String getDiseaseHistory() {
        return diseaseHistory;
    }

    public void setDiseaseHistory(String diseaseHistory) {
        this.diseaseHistory = diseaseHistory;
    }
}
