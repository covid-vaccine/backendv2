package com.outlanders.cetas.domain;

public class Ticket {
    private Integer ticket_id;
    private Integer registrant_id;
    private String location;
    private Integer attempt_number;
    private String fee;

    public Ticket(Integer ticket_id, Integer registrant_id, String location, Integer attempt_number, String fee) {
        this.ticket_id = ticket_id;
        this.registrant_id = registrant_id;
        this.location = location;
        this.attempt_number = attempt_number;
        this.fee = fee;
    }

    public Integer getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(Integer ticket_id) {
        this.ticket_id = ticket_id;
    }

    public Integer getRegistrant_id() {
        return registrant_id;
    }

    public void setRegistrant_id(Integer registrant_id) {
        this.registrant_id = registrant_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getAttempt_number() {
        return attempt_number;
    }

    public void setAttempt_number(Integer attempt_number) {
        this.attempt_number = attempt_number;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }
}
