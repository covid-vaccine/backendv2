package com.outlanders.cetas;

import com.outlanders.cetas.filters.UserAuthFilter;
import com.outlanders.cetas.storage.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableConfigurationProperties(StorageProperties.class)
public class CetasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CetasApplication.class, args);
	}

	@Bean
	public FilterRegistrationBean<UserAuthFilter> userRegistrationBean() {
		FilterRegistrationBean<UserAuthFilter> registrationBean = new FilterRegistrationBean<>();
		UserAuthFilter userAuthFilter = new UserAuthFilter();
		registrationBean.setFilter(userAuthFilter);
		registrationBean.addUrlPatterns("/api/registrant/*");
		return registrationBean;
	}

}
