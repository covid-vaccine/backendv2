package com.outlanders.cetas.services.registration;


import com.outlanders.cetas.domain.Registrant;
import com.outlanders.cetas.domain.Result;
import com.outlanders.cetas.exceptions.UlBadRequestException;

import java.util.List;

public interface RegistrationService {

    List<Registrant> fetchAllRegistrants();

    List<Result> fetchAllTicketByUser(Integer userId);

    Result fetchTicketById(Integer ticketId);

    Result addRegistrant(Integer userId, String registrantName, String gender, String nik, Integer age, String address, String workType, Integer workDuration, String diseaseHistory) throws UlBadRequestException;
}
