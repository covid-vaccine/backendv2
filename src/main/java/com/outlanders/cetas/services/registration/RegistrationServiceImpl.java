package com.outlanders.cetas.services.registration;

import com.outlanders.cetas.domain.Registrant;
import com.outlanders.cetas.domain.Result;
import com.outlanders.cetas.exceptions.UlBadRequestException;
import com.outlanders.cetas.repositories.registration.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    RegistrationRepository registrationRepository;

    @Override
    public List<Registrant> fetchAllRegistrants() {
        return registrationRepository.findAll();
    }

    @Override
    public List<Result> fetchAllTicketByUser(Integer userId) {
        return registrationRepository.findTicketByUserId(userId);
    }

    @Override
    public Result fetchTicketById(Integer ticketId) {
        return registrationRepository.findTicketById(ticketId);
    }

    @Override
    public Result addRegistrant(Integer userId, String registrantName, String gender, String nik, Integer age, String address, String workType, Integer workDuration, String diseaseHistory) throws UlBadRequestException {
        int ticketId = registrationRepository.create(userId, registrantName, gender, nik, age, address, workType, workDuration, diseaseHistory);
        return registrationRepository.findTicketById(ticketId);
    }
}
