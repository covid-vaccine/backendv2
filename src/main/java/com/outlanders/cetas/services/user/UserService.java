package com.outlanders.cetas.services.user;

import com.outlanders.cetas.domain.User;
import com.outlanders.cetas.exceptions.UlAuthException;

public interface UserService {
    User validateUser(String email, String password) throws UlAuthException;

    User getUserById(Integer userId) throws UlAuthException;


    User registerUser(String firstName, String lastName, String email, String password) throws UlAuthException;
}
