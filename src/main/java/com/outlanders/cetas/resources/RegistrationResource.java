package com.outlanders.cetas.resources;

import com.outlanders.cetas.domain.Registrant;
import com.outlanders.cetas.domain.Result;
import com.outlanders.cetas.domain.User;
import com.outlanders.cetas.services.registration.RegistrationService;
import com.outlanders.cetas.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/registrant")
public class RegistrationResource {

    @Autowired
    RegistrationService registrationService;

    @Autowired
    UserService userService;

    @GetMapping("")
    public ResponseEntity<List<Registrant>> getAllRegistrants() {
        List<Registrant> registrants = registrationService.fetchAllRegistrants();
        return new ResponseEntity<>(registrants, HttpStatus.OK);
    }

    @GetMapping("/tickets")
    public ResponseEntity<List<Result>> getAllTicketByUser(HttpServletRequest request) {
        Integer userId = (Integer) request.getAttribute("userId");
        List<Result> results = registrationService.fetchAllTicketByUser(userId);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @GetMapping("/ticket/{id}")
    public ResponseEntity<Result> getTicketById(@PathVariable Integer id) {
        Result result = registrationService.fetchTicketById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/user")
    public User getUserById(HttpServletRequest request) {
        Integer userId = (Integer) request.getAttribute("userId");
        return userService.getUserById(userId);
    }

    @PostMapping("/registration")
    public ResponseEntity<Result> addRegistrant(HttpServletRequest request, @RequestBody Map<String, Object> userMap) {
        Integer userId = (Integer) request.getAttribute("userId");
        String name = (String) userMap.get("name");
        String gender = (String) userMap.get("gender");
        String nik = (String) userMap.get("nik");
        Integer age = (Integer) userMap.get("age");
        String address = (String) userMap.get("address");
        String workType = (String) userMap.get("work_type");
        Integer workDuration = (Integer) userMap.get("work_duration");
        String diseaseHistory = (String) userMap.get("disease_history");
        System.out.println(name);
        Result result = registrationService.addRegistrant(userId, name, gender, nik,
                age, address, workType, workDuration, diseaseHistory);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
