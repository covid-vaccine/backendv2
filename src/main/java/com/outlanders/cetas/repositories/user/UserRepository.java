package com.outlanders.cetas.repositories.user;

import com.outlanders.cetas.domain.User;
import com.outlanders.cetas.exceptions.UlAuthException;

public interface UserRepository {
    Integer create(String firstName, String lastName, String email, String password) throws UlAuthException;

    User findByEmailAndPassword(String email, String password) throws UlAuthException;

    Integer getCountByEmail(String email);

    User findById(Integer userId);
}
