package com.outlanders.cetas.repositories.registration;

import com.outlanders.cetas.domain.Registrant;
import com.outlanders.cetas.domain.Result;
import com.outlanders.cetas.exceptions.UlBadRequestException;
import com.outlanders.cetas.exceptions.UlResourceNotFoundException;

import java.util.List;

public interface RegistrationRepository {

    List<Registrant> findAll();

    Registrant findById(Integer registrantId) throws UlResourceNotFoundException;

    Integer create(Integer user_id, String registrantName, String gender, String nik, Integer age, String address, String workType, Integer workDuration, String diseaseHistory) throws UlBadRequestException;

    Result findTicketById(Integer ticketId) throws UlResourceNotFoundException;

    List<Result> findTicketByUserId(Integer userId) throws UlResourceNotFoundException;

}
