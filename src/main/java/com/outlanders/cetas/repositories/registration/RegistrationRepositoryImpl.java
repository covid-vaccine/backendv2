package com.outlanders.cetas.repositories.registration;

import com.outlanders.cetas.domain.Registrant;
import com.outlanders.cetas.domain.Result;
import com.outlanders.cetas.exceptions.UlBadRequestException;
import com.outlanders.cetas.exceptions.UlResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class RegistrationRepositoryImpl implements RegistrationRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SQL_FIND_ALL = "SELECT * FROM UL_REGISTRANTS";
    private static final String SQL_FIND_BY_ID = "SELECT * FROM Ul_REGISTRANTS WHERE REGISTRANT_ID = ?";
    private static final String SQl_FIND_TICKET_BY_ID = "select ticket_id, registrant_name, nik, address, email, location, attempt_number, release_date, fee from (ul_users a inner join ul_registrants b on a.user_id=b.user_id) inner join ul_tickets c on b.registrant_id=c.registrant_id where c.ticket_id=?";
    private static final String SQL_CREATE = "INSERT INTO Ul_REGISTRANTS(registrant_id, user_id, registrant_name, gender, nik, age, address, work_type, work_duration, disease_history) VALUES(NEXTVAL('ul_registrants_seq'), ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_CREATE_TICKET = "INSERT INTO UL_TICKETS(ticket_id, registrant_id, location, attempt_number, release_date, fee) VALUES(NEXTVAL('ul_tickets_seq'), ?, ?, ?, ?, ?)";
    private static final String SQl_FIND_TICKET_BY_USER_ID = "select ticket_id, registrant_name, nik, address, email, location, attempt_number, release_date, fee from (ul_users a inner join ul_registrants b on a.user_id=b.user_id) inner join ul_tickets c on b.registrant_id=c.registrant_id where a.user_id=?";


    @Override
    public List<Registrant> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL, registrantRowMapper);
    }

    @Override
    public Registrant findById(Integer registrantId) throws UlResourceNotFoundException {
        try {
            return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new Object[]{registrantId}, registrantRowMapper);
        }catch (Exception e) {
            throw new UlResourceNotFoundException("Course not found");
        }
    }

    @Override
    public Integer create(Integer userId, String registrantName, String gender, String nik, Integer age, String address, String workType, Integer workDuration, String diseaseHistory) throws UlBadRequestException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, userId);
                ps.setString(2, registrantName);
                ps.setString(3, gender);
                ps.setString(4, nik);
                ps.setInt(5, age);
                ps.setString(6, address);
                ps.setString(7, workType);
                ps.setInt(8, workDuration);
                ps.setString(9, diseaseHistory);
                return ps;
            }, keyHolder);
            Integer registrantId = (Integer) keyHolder.getKeys().get("registrant_id");

            long millis=System.currentTimeMillis();
            Date date = new Date(millis);
            KeyHolder keyHolder2 = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE_TICKET, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, registrantId);
                ps.setString(2, "Fasilitas Kesehatan Depok");
                ps.setInt(3, 3);
                ps.setDate(4, date);
                ps.setString(5, "Gratis");
                return ps;
            }, keyHolder2);

            return (Integer) keyHolder2.getKeys().get("ticket_id");
        } catch (Exception e) {
            throw new UlBadRequestException("Invalid request");
        }
    }

    @Override
    public Result findTicketById(Integer ticketId) throws UlResourceNotFoundException {
        try {
            return jdbcTemplate.queryForObject(SQl_FIND_TICKET_BY_ID, new Object[]{ticketId}, resultRowMapper);
        }catch (Exception e) {
            throw new UlResourceNotFoundException("Course not found");
        }
    }

    @Override
    public List<Result> findTicketByUserId(Integer userId) throws UlResourceNotFoundException {
        try {
            return jdbcTemplate.query(SQl_FIND_TICKET_BY_USER_ID, new Object[]{userId}, resultRowMapper);
        }catch (Exception e) {
            throw new UlResourceNotFoundException("Course not found");
        }
    }

    private RowMapper<Registrant> registrantRowMapper = ((rs, rowNum) -> {
        return new Registrant(
                rs.getInt("REGISTRANT_ID"),
                rs.getInt("USER_ID"),
                rs.getString("REGISTRANT_NAME"),
                rs.getString("GENDER"),
                rs.getString("NIK"),
                rs.getInt("AGE"),
                rs.getString("ADDRESS"),
                rs.getString("WORK_TYPE"),
                rs.getInt("WORK_DURATION"),
                rs.getString("DISEASE_HISTORY")
        );
    });

    private RowMapper<Result> resultRowMapper = ((rs, rowNum) -> {
        return new Result(
                rs.getInt("TICKET_ID"),
                rs.getString("REGISTRANT_NAME"),
                rs.getString("NIK"),
                rs.getString("ADDRESS"),
                rs.getString("EMAIL"),
                rs.getString("LOCATION"),
                rs.getInt("ATTEMPT_NUMBER"),
                rs.getDate("RELEASE_DATE"),
                rs.getString("FEE")
        );
    });
}
